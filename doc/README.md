# Nexa Specification

The comprehensive specification of the Nexa protocol could be found at either of the following:
- [Specification Gitlab](https://gitlab.com/nexa/specfication)
- [Specification Website](https://spec.nexa.org)
