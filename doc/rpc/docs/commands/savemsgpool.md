```
savemsgpool

Dumps the CAPD msgpool to disk.

Examples:
> nexa-cli savemsgpool 
> curl --user myusername --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "savemsgpool", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:7227/

```
