Release Notes for Nexa 1.4.0.1
======================================================

Nexa version 1.4.0.1  is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at gitlab:

  <https://gitlab.com/nexa/nexa/-/issues>

This is mainly a bug fix release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.4.0.1
-----------------------

This is list of the main changes that have been merged in this release:

- Initialize the max send/receive buffer sizes on startup rather than initialise them each time we need them
- In two cases ask fort `resync` rather than `reindex` if pruned mode is on
- Add a note about performance on `regtest`
- Fix an edge case display error in the token history page when a new token is created and then minted before the next block is mined
- Fix a token wallet issue that won't let you broadcast transactions to send minted/received tokens

Commit details
--------------

- `d84e96163` Add release notes for Nexa version 1.4.0.1 (Andrea Suisani)
- `40ce331b1` Bump Nexa version to 1.4.0.1 (Andrea Suisani)
- `c4a761267` Improve the performance of accumulating authorities and mintages (Peter Tschipper)
- `5152f7eb9` GROUPED_SATOSHI_AMT was not getting properly initialized (Peter Tschipper)
- `c2fd39e59` Initialize the max send and receive buffer sizes on startup (Peter TSchipper)
- `f90d81ce5` Two messages for when in pruned mode were incorrectly asking for -reindex (Peter Tschipper)
- `a81da7174` Add NOTE about performance on regtest (Peter Tschipper)
- `2278f76c9` Fix bug in token history where the decimals don't update correctly (Peter Tschipper)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Peter Tschipper

