Nexa
====

Intro
-----
Nexa is a free open source peer-to-peer electronic cash system that is
completely decentralized, without the need for a central server or trusted
parties.  Users hold the crypto keys to their own money and transact directly
with each other, with the help of a P2P network to check for double-spending.


Setup
-----
There are two ways to install Nexa on windows. You can either unpack the zip file
into a directory and run nexa-qt.exe, or you can install using the *.exe install
program which will put nexa-qt.exe within C:\users\<your account\appdata\roaming\nexa.
If you want to add startup parameters you can create a config file "nexa.conf"
within the corresponding install directory and add parameters as needed.

The Nexa node creates the backbone of the P2P network.
However, it downloads and stores the entire history of all Nexa transactions;
depending on the speed of your computer and network connection, the synchronization
process can take anywhere from a few hours to a day or more.


Online resources
----------------

The Bitcoin Unlimited website: https://www.bitcoinunlimited.info/
The Nexa website : https://nexa.org
