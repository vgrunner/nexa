---
name: "nexa-win"
enable_cache: true
suites:
- "focal"
architectures:
- "amd64"
packages:
- "curl"
- "g++"
- "git"
- "pkg-config"
- "autoconf"
- "libtool"
- "automake"
- "faketime"
- "bsdmainutils"
- "mingw-w64"
- "g++-mingw-w64"
- "nsis"
- "zip"
- "ca-certificates"
- "python"
- "openjdk-17-jdk-headless"
- "rename"
reference_datetime: "2020-08-20 00:00:00"
remotes:
- "url": "https://gitlab.com/nexa/nexa.git"
  "dir": "nexa"
files: []
script: |
  set -e -o pipefail

  if [ "${ONLY_LIBNEXA_ARG}" != "--enable-only-libnexa" ]; then
    JAVA_LIBNEXA="--enable-javalibnexa"
  fi

  WRAP_DIR=$HOME/wrapped
  HOSTS="x86_64-w64-mingw32"
  CONFIGFLAGS="--enable-reduce-exports --disable-bench --disable-gui-tests --disable-tests --enable-shared ${JAVA_LIBNEXA} ${ONLY_LIBNEXA_ARG}"
  FAKETIME_HOST_PROGS="ar ranlib nm windres strip"
  FAKETIME_PROGS="date makensis zip"

  export TAR_OPTIONS="--mtime="$REFERENCE_DATE\\\ $REFERENCE_TIME""
  export TZ="UTC"
  export BUILD_DIR=`pwd`
  mkdir -p ${WRAP_DIR}
  if test -n "$GBUILD_CACHE_ENABLED"; then
    export SOURCES_PATH=${GBUILD_COMMON_CACHE}
    export BASE_CACHE=${GBUILD_PACKAGE_CACHE}
    mkdir -p ${BASE_CACHE} ${SOURCES_PATH}
  fi

  # Create global faketime wrappers
  for prog in ${FAKETIME_PROGS}; do
    echo '#!/bin/bash' > ${WRAP_DIR}/${prog}
    echo "REAL=\`which -a ${prog} | grep -v ${WRAP_DIR}/${prog} | head -1\`" >> ${WRAP_DIR}/${prog}
    echo 'export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1' >> ${WRAP_DIR}/${prog}
    echo "export FAKETIME=\"${REFERENCE_DATETIME}\"" >> ${WRAP_DIR}/${prog}
    echo "exec \"\$REAL\" \"\$@\"" >> ${WRAP_DIR}/${prog}
    chmod +x ${WRAP_DIR}/${prog}
  done

  # Create per-host faketime wrappers
  for i in $HOSTS; do
    for prog in ${FAKETIME_HOST_PROGS}; do
        echo '#!/bin/bash' > ${WRAP_DIR}/${i}-${prog}
        echo "REAL=\`which -a ${i}-${prog} | grep -v ${WRAP_DIR}/${i}-${prog} | head -1\`" >> ${WRAP_DIR}/${i}-${prog}
        echo 'export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1' >> ${WRAP_DIR}/${i}-${prog}
        echo "export FAKETIME=\"${REFERENCE_DATETIME}\"" >> ${WRAP_DIR}/${i}-${prog}
        echo "exec \"\$REAL\" \"\$@\"" >> ${WRAP_DIR}/${i}-${prog}
        chmod +x ${WRAP_DIR}/${i}-${prog}
    done
  done

  # Create per-host linker wrapper
  # This is only needed for trusty, as the mingw linker leaks a few bytes of
  # heap, causing non-determinism. See discussion in https://github.com/bitcoin/bitcoin/pull/6900
  for i in $HOSTS; do
    mkdir -p ${WRAP_DIR}/${i}
    for prog in collect2; do
        echo '#!/bin/bash' > ${WRAP_DIR}/${i}/${prog}
        REAL=$(${i}-gcc -print-prog-name=${prog})
        echo "export MALLOC_PERTURB_=255" >> ${WRAP_DIR}/${i}/${prog}
        echo "${REAL} \$@" >> $WRAP_DIR/${i}/${prog}
        chmod +x ${WRAP_DIR}/${i}/${prog}
    done
    for prog in gcc g++; do
        echo '#!/bin/bash' > ${WRAP_DIR}/${i}-${prog}
        echo "REAL=\`which -a ${i}-${prog}-posix | grep -v ${WRAP_DIR}/${i}-${prog} | head -1\`" >> ${WRAP_DIR}/${i}-${prog}
        echo 'export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1' >> ${WRAP_DIR}/${i}-${prog}
        echo "export FAKETIME=\"${REFERENCE_DATETIME}\"" >> ${WRAP_DIR}/${i}-${prog}
        echo "export COMPILER_PATH=${WRAP_DIR}/${i}" >> ${WRAP_DIR}/${i}-${prog}
        echo "\$REAL \$@" >> $WRAP_DIR/${i}-${prog}
        chmod +x ${WRAP_DIR}/${i}-${prog}
    done
  done

  export PATH=${WRAP_DIR}:${PATH}
  export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64

  cd nexa
  BASEPREFIX=`pwd`/depends
  DEPENDS_LIBNEXA="ONLY_LIBNEXA=0"
  if [ "${ONLY_LIBNEXA_ARG}" = "--enable-only-libnexa" ]; then
    DEPENDS_LIBNEXA="ONLY_LIBNEXA=1"
  fi
  # Build dependencies for each host
  for i in $HOSTS; do
    make ${MAKEOPTS} -C ${BASEPREFIX} HOST="${i}" NO_RUST=1 ${DEPENDS_LIBNEXA}
  done

  # Create the release tarball using (arbitrarily) the first host
  ./autogen.sh
  ./configure ${ONLY_LIBNEXA_ARG} --prefix=${BASEPREFIX}/`echo "${HOSTS}" | awk '{print $1;}'`
  make dist
  SOURCEDIST=`echo nexa-*.tar.gz`
  DISTNAME=`echo ${SOURCEDIST} | sed 's/.tar.*//'`

  # Correct tar file order
  mkdir -p temp
  pushd temp
  tar xf ../$SOURCEDIST
  find nexa* | sort | tar --no-recursion --mode='u+rw,go+r-w,a+X' --owner=0 --group=0 -c -T - | gzip -9n > ../$SOURCEDIST
  mkdir -p $OUTDIR/src
  cp ../$SOURCEDIST $OUTDIR/src
  popd

  ORIGPATH="$PATH"
  # Extract the release tarball into a dir for each host and build
  for i in ${HOSTS}; do
    # ORIGPATH must be first because we need to grab the wrapped g++ to ensure we get the proper threading model (posix or windows)
    export PATH=${ORIGPATH}:${BASEPREFIX}/${i}/native/bin
    mkdir -p distsrc-${i}
    cd distsrc-${i}
    INSTALLPATH=`pwd`/installed/${DISTNAME}
    mkdir -p ${INSTALLPATH}
    echo BUILDING NEXA for ${i} in `pwd` to be installed into ${INSTALLPATH}
    cp ../INSTALL.md ${INSTALLPATH}
    tar --strip-components=1 -xf ../$SOURCEDIST

    ./configure --prefix=${BASEPREFIX}/${i} --bindir=${INSTALLPATH}/bin --includedir=${INSTALLPATH}/include --libdir=${INSTALLPATH}/lib --disable-ccache --disable-maintainer-mode --disable-dependency-tracking ${CONFIGFLAGS}
    echo "removing -DDLL_EXPORT from libtool via: sed -i 's/-DDLL_EXPORT//g' libtool"
    sed -i 's/-DDLL_EXPORT//g' libtool
    make V=1 ${MAKEOPTS}
    make ${MAKEOPTS} -C src check-security
    make ${MAKEOPTS} -C src check-symbols
    if [ "${ONLY_LIBNEXA_ARG}" != "--enable-only-libnexa" ]; then
      make deploy
      if [ -f nexa*setup*.exe ]; then
        cp -f nexa*setup*.exe $OUTDIR/
      fi
    fi
    make install-strip
    cd installed
    # fix up the name; its being generated as libnexa-0.dll
    (cd */bin; mv libnexa-0.dll libnexa.dll)
    find . -name "lib*.la" -delete
    find . -name "lib*.a" -delete
    rm -rf ${DISTNAME}/lib/pkgconfig
    find ${DISTNAME} -type f | sort | zip -X@ ${OUTDIR}/${DISTNAME}-${i}.zip
    cd ../..
  done
  cd $OUTDIR
  rename 's/-setup\.exe$/-setup-unsigned.exe/' *-setup.exe
  find . -name "*-setup-unsigned.exe" | sort | tar --no-recursion --mode='u+rw,go+r-w,a+X' --owner=0 --group=0 -c -T - | gzip -9n > ${OUTDIR}/${DISTNAME}-win-unsigned.tar.gz
  mv ${OUTDIR}/${DISTNAME}-x86_64-*.zip ${OUTDIR}/${DISTNAME}-win64.zip
